// Program.java

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontFormatException;
//import java.util.Locale;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;

public class Program
{
	public static void main(String[] args)
	{
		try
		{
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("font/Consola.ttf")));
			/*
			for (String font : ge.getAvailableFontFamilyNames())
			{
				System.out.println(font);
			}*/
		}
		catch (IOException|FontFormatException e)
		{
			
		}
		
		//Locale locale = Locale.getDefault(); // Get JVM Locale
		
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					UserInterface ui = new UserInterface();
					ui.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
