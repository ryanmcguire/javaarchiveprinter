// UserInterface.java

import javax.swing.JFileChooser;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.UIManager;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Enumeration;
import java.util.zip.ZipException;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FontUIResource;

import java.awt.Color;
import javax.swing.JCheckBox;

public class UserInterface extends JFrame
{
	private static final long serialVersionUID = 7883635252923313074L;
	private JTextArea txtAreaPrintJAR = null;
	private JTextField txtSelectJAR = null;
	private JFileChooser fileChooser = null;
	private FileNameExtensionFilter filterJavaArchive = null;
	private FileNameExtensionFilter filterTextFile = null;
	private JCheckBox chckbxIncludeFilename;
	private JCheckBox chckbxVerbose;
	private JCheckBox chckbxAutoClear;
	private JCheckBox chckbxIncludePath;
	
	public UserInterface()
	{
		setUIFont(new FontUIResource("Consolas", Font.PLAIN, 12));
		setResizable(false);
		setTitle("JAR Printer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100,475, 335);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		
		fileChooser = new JFileChooser(); // Open to User's Default Directory
		//fileChooser = new JFileChooser(System.getProperty("user.dir")); // Open to User's Working Directory
		filterJavaArchive = new FileNameExtensionFilter("Java Archive (*.jar)", "jar");
		filterTextFile = new FileNameExtensionFilter("Normal text file (*.txt)", "txt");
		
		JButton btnSelectJAR = new JButton("Select JAR...");
		btnSelectJAR.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				fileChooser.resetChoosableFileFilters();
				fileChooser.setAcceptAllFileFilterUsed(false);
				fileChooser.setFileFilter(filterJavaArchive);
				File selectedFile = txtSelectJAR.getText().trim().equals("") ? null : new File(txtSelectJAR.getText());
				fileChooser.setSelectedFile(selectedFile);
				if (fileChooser.showOpenDialog(UserInterface.this) == JFileChooser.APPROVE_OPTION) 
				{
					File file = fileChooser.getSelectedFile();
					txtSelectJAR.setText(file.getAbsolutePath());
				}
			}
		});
		btnSelectJAR.setBounds(335, 11, 125, 23);
		getContentPane().add(btnSelectJAR);
		
		JButton btnPrintJAR = new JButton("Print JAR");
		btnPrintJAR.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if (!txtSelectJAR.getText().trim().equals(""))
				{
					File file = new File(txtSelectJAR.getText());
					try
					{
						String newText = JARData.GetContents(file, chckbxIncludeFilename.isSelected(),
								chckbxIncludePath.isEnabled() ? chckbxIncludePath.isSelected() : false,
								chckbxVerbose.isSelected());
						if (!chckbxAutoClear.isSelected())
						{
							txtAreaPrintJAR.setText((txtAreaPrintJAR.getText().isEmpty() ? "" : txtAreaPrintJAR.getText() + Globals.NEW_LINE)
									+ newText);
						}
						else
						{
							txtAreaPrintJAR.setText(newText);
						}
					}
					catch (ZipException e1)
					{
						JOptionPane.showMessageDialog(UserInterface.this,
								"Could not open \'" + txtSelectJAR.getText() + "\' as a JAR file.",
								UserInterface.this.getTitle(), JOptionPane.ERROR_MESSAGE);
					}
					catch (IOException e1)
					{
						JOptionPane.showMessageDialog(UserInterface.this,
								"Failed to open \'" + txtSelectJAR.getText() + "\'.",
								UserInterface.this.getTitle(), JOptionPane.ERROR_MESSAGE);
					}
					catch (SecurityException e1)
					{
						JOptionPane.showMessageDialog(UserInterface.this,
								"Failed to open \'" + txtSelectJAR.getText() + "\' due to permissions.",
								UserInterface.this.getTitle(), JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnPrintJAR.setBounds(10, 247,105, 23);
		getContentPane().add(btnPrintJAR);
		
		txtSelectJAR = new JTextField();
		txtSelectJAR.setBackground(Color.WHITE);
		txtSelectJAR.setEditable(false);
		txtSelectJAR.setBounds(10, 11, 315, 23);
		getContentPane().add(txtSelectJAR);
		txtSelectJAR.setColumns(10);
		
		JButton btnSaveAs = new JButton("Save As...");
		btnSaveAs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int result = JOptionPane.YES_OPTION;
				if (txtAreaPrintJAR.getText().length() == 0)
				{
					result = JOptionPane.showConfirmDialog(UserInterface.this,
							"No print output found. Are you sure you want to save?",
							UserInterface.this.getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
				}
				if (result == JOptionPane.YES_OPTION)
				{
					fileChooser.resetChoosableFileFilters();
					fileChooser.setAcceptAllFileFilterUsed(true);
					fileChooser.setFileFilter(filterTextFile);
					fileChooser.setSelectedFile(new File(txtSelectJAR.getText() + ".txt"));
					if (fileChooser.showSaveDialog(UserInterface.this) == JFileChooser.APPROVE_OPTION) 
					{
						boolean authorized = true;
						File file = fileChooser.getSelectedFile();
						if (file.exists())
						{
							result = JOptionPane.showConfirmDialog(UserInterface.this,
									file.getAbsoluteFile() + " already exists.\nDo you want to replace it?",
									UserInterface.this.getTitle(), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
							authorized = (result == JOptionPane.YES_OPTION);
						}
						
						if (authorized)
						{
							try
							{
								OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file));
								osw.write(txtAreaPrintJAR.getText()); // Remove all trailing whitespace
								osw.close();
							}
							catch (FileNotFoundException e1)
							{
								
							}
							catch (IOException e1)
							{
								
							}
						}
					}
				}
			}
		});

		btnSaveAs.setBounds(240, 247,105, 23);
		getContentPane().add(btnSaveAs);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				txtAreaPrintJAR.setText("");				
			}
		});
		btnClear.setBounds(125, 247,105, 23);
		getContentPane().add(btnClear);
		
		JScrollPane scrollPaneOutput = new JScrollPane();
		scrollPaneOutput.setBounds(10, 45, 450, 192);
		getContentPane().add(scrollPaneOutput);
		
		txtAreaPrintJAR = new JTextArea();
		txtAreaPrintJAR.setEditable(false);
		scrollPaneOutput.setViewportView(txtAreaPrintJAR);
		
		JButton btnCopy = new JButton("Copy");
		btnCopy.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Clipboard clipBoard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipBoard.setContents(new StringSelection(txtAreaPrintJAR.getText()), null);
			}
		});
		btnCopy.setBounds(355, 247,105, 23);
		getContentPane().add(btnCopy);
		
		chckbxVerbose = new JCheckBox("Verbose");
		chckbxVerbose.setBounds(10, 277, 97, 23);
		getContentPane().add(chckbxVerbose);
		
		chckbxIncludeFilename = new JCheckBox("Include Filename");
		chckbxIncludeFilename.setBounds(207, 277, 138, 23);
		chckbxIncludeFilename.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if (((JCheckBox)e.getSource()).isSelected())
				{
					chckbxIncludePath.setEnabled(true);
				}
				else
				{
					chckbxIncludePath.setEnabled(false);
				}
			}
		});
		getContentPane().add(chckbxIncludeFilename);
		
		chckbxAutoClear = new JCheckBox("AutoClear");
		chckbxAutoClear.setSelected(true);
		chckbxAutoClear.setBounds(109, 277, 97, 23);
		getContentPane().add(chckbxAutoClear);
		
		chckbxIncludePath = new JCheckBox("Include Path");
		chckbxIncludePath.setEnabled(false);
		chckbxIncludePath.setBounds(346, 277, 114, 23);
		getContentPane().add(chckbxIncludePath);
	}
	
	public void setUIFont (FontUIResource fontResource)
	{
		Enumeration<Object> keys = UIManager.getDefaults().keys();
		while (keys.hasMoreElements())
		{
			Object key = keys.nextElement();
			Object value = UIManager.get(key);
			if (value != null && value instanceof FontUIResource)
			{
				UIManager.put(key, fontResource);
			}
		}
	} 
}
