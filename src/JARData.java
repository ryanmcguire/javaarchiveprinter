// JARData.java

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipException;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class JARData
{
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("EEE MMM dd kk:mm:ss zzz yyyy");
	
	public JARData()
	{
		
	}
	
	public static String GetContents(File file, boolean includeFilename, boolean includePath, boolean verbose) throws ZipException, IOException, SecurityException
	{
		StringBuilder contents = new StringBuilder();
		int topSizeStringLength = 0;
		JarFile jar = new JarFile(file);
		Enumeration<JarEntry> jarEntries = jar.entries();
		
		if (includeFilename)
		{
			topSizeStringLength =  String.valueOf(file.length()).length();
			contents.append(formatOutput(file, includePath, verbose));
		}
		
		while(jarEntries.hasMoreElements())
		{
			JarEntry entry = jarEntries.nextElement();
			
			if (verbose)
			{
				String fileSize = String.valueOf(entry.getSize());
				int diff = topSizeStringLength - fileSize.length();
				if (diff < 0) // Update previous lines for longer size value String
				{
					topSizeStringLength = fileSize.length();
					String[] lines = contents.toString().split(Globals.NEW_LINE);
					if (lines.length > 0 && !lines[0].isEmpty())
					{
						StringBuilder builder = new StringBuilder();
						for (String line : lines)
						{
							builder.append(String.format("%" + (-diff) + "s", "") + line + Globals.NEW_LINE);
						}
						contents = builder; // Set Contents to updated StringBuilder
					}
					
					contents.append(formatOutput(entry, 0, verbose));
				}
				else // Append Normally
				{
					contents.append(formatOutput(entry, diff, verbose));
				}
			}
			else // Non-Verbose
			{
				contents.append(formatOutput(entry));
			}
		}
		
		jar.close();
		
		return contents.toString();
	}
	
	private static String formatOutput(JarEntry entry)
	{
		return entry.toString() + Globals.NEW_LINE;
	}
	
	private static String formatOutput(JarEntry entry, int numOfSpaces, boolean verbose)
	{
		if (verbose)
		{
			String spaces = (numOfSpaces == 0) ? "" : String.format("%" + numOfSpaces + "s", "");
			
			return spaces + entry.getSize() + " " +
					dateTimeFormatter.print(new DateTime(entry.getTime(), DateTime.now().getZone())) +
					" " + formatOutput(entry);
		}
		else
		{
			return formatOutput(entry);
		}
	}
	
	private static String formatOutput(File file, boolean includePath)
	{
		if (includePath)
		{
			return file.getAbsolutePath() + Globals.NEW_LINE;
		}
		else
		{
			return file.getName() + Globals.NEW_LINE;
		}
	}
	
	private static String formatOutput(File file, boolean includePath, boolean verbose)
	{
		if (verbose)
		{
			return file.length() + " " +
				dateTimeFormatter.print(new DateTime(file.lastModified(), DateTime.now().getZone())) +
				" " + formatOutput(file, includePath);
		}
		else
		{
			return formatOutput(file, includePath);
		}
	}
}
